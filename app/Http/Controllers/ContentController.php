<?php

namespace App\Http\Controllers;


use App\Content;

class ContentController extends Controller
{

    public function __construct()
    {

    }

    public function index() {

        $contents = Content::tree();
        return view('content', ['contents' => $this->htmlView($contents)]);
    }


    public function htmlView($contents) {

        $markup = '';

        foreach($contents as $content) {

            $markup .= '<li>'. $content->children->isNotEmpty() ?
                'U: '.$content->user_name . ' / L: ' . $content->credits_left . ' / R: ' . $content->credits_right . $this->htmlView($content->children) :
                $content->children->user_name  .'</li>';
        }

        return '<ul>'. $markup .'</ul>';
    }



}
