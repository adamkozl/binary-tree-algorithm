<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{

    protected $table = 'contents';

    protected $fillable = [
        'user_name',
        'credits_left',
        'credits_right',
        'parent_id',
    ];



    public function parent() {

        return $this->hasOne('App\Content', 'id', 'parent_id');

    }

    public function children() {

        return $this->hasMany('App\Content', 'parent_id', 'id');

    }

    public static function tree() {

        return static::with(implode('.', array_fill(0, 4, 'children')))->where('parent_id', '=', NULL)->get();

    }


}
