<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /*$modelOfFirstRecord = DB::table('contents')->insert([
            'user_name' => rand(1,10),
            'credits_left' => rand(1,1000),
            'credits_right' => rand(1,1000),
            'parent_id' => null
        ]);

        DB::table('contents')->insert([
            'user_name' => rand(1,10),
            'credits_left' => rand(1,1000),
            'credits_right' => rand(1,1000),
            'parent_id' => $modelOfFirstRecord['id']
        ]);*/


        $root = \App\Content::create([
            'user_name' => rand(1,10),
            'credits_left' => rand(1,1000),
            'credits_right' => rand(1,1000),
            'parent_id' => null
        ]);


        $firstChild = \App\Content::create([
            'user_name' => rand(1,10),
            'credits_left' => rand(1,1000),
            'credits_right' => rand(1,1000),
            'parent_id' => $root->id
        ]);

        $secondChild = \App\Content::create([
            'user_name' => rand(1,10),
            'credits_left' => rand(1,1000),
            'credits_right' => rand(1,1000),
            'parent_id' => $root->id
        ]);


        $firstChildOfFirstChild = \App\Content::create([
            'user_name' => rand(1,10),
            'credits_left' => rand(1,1000),
            'credits_right' => rand(1,1000),
            'parent_id' => $firstChild->id
        ]);

        $secondChildOfFirstChild = \App\Content::create([
            'user_name' => rand(1,10),
            'credits_left' => rand(1,1000),
            'credits_right' => rand(1,1000),
            'parent_id' => $firstChild->id
        ]);


        $firstChildOfSecondChild = \App\Content::create([
            'user_name' => rand(1,10),
            'credits_left' => rand(1,1000),
            'credits_right' => rand(1,1000),
            'parent_id' => $secondChild->id
        ]);

        $secondChildOfSecondChild = \App\Content::create([
            'user_name' => rand(1,10),
            'credits_left' => rand(1,1000),
            'credits_right' => rand(1,1000),
            'parent_id' => $secondChild->id
        ]);

        $firstChildOfSecondChildOfFirstChild = \App\Content::create([
            'user_name' => rand(1,10),
            'credits_left' => rand(1,1000),
            'credits_right' => rand(1,1000),
            'parent_id' => $secondChildOfFirstChild->id
        ]);
    }
}
