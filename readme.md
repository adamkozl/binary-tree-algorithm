This is Binary Tree Algorithm implementation based on PHP Laravel 5.8 framework.

You can use this kind of algorithm for dynamic menu or tree of dependency. By traversing you can retrive any element with its children and parent

Requirements: PHP 7.1 or larger and Composer

1. Clone project

2. Go to project file and install dependencies <br/>

composer install <br/>

3. In project copy ".env.example" and name it ".env"

4. In ".env" set database connection:


DB_DATABASE=your_local_db_name

DB_USERNAME=your_user_name

DB_PASSWORD=your_pass


5. Set Laravel application key by command:

php artisan key:generate <br/>

6. Clear application cache:

php artisan config:cache <br/>


7. Create database structure:

php artisan migrate <br/>


8. Make database seeds with basic data:

php artisan db:seed <br/>


9. Fire virtual server by command:

php artisan serve <br/>


10. Project is ready to use. You can see the binary structure under address: localhost:8000


PROJECT STRCTURE:

- ROUTES: /routes/web.php
- CONTROLLERS: /app/Http/Controllers/ContentController.php
- DB MIGRATIONS: /database/migrations
- DB SEEDS: /database/seeds

Developer: Adam Kozlowski / adam@coolcode.pl
Last update: 26.04.2019